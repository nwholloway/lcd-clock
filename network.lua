local statusCallback

local reconnectTimer = tmr.create()

local function status(text)
    print(text)
    statusCallback(text)
end

local function reconnectAfter(delay)
    reconnectTimer:interval(delay)
    reconnectTimer:start()
end

local function ntpSync()
    local function success(_, _, server, info)
        local offset = info['offset_s'] and (info['offset_s'] .. 's') or (info['offset_us'] .. 'us')
        print(string.format('SNTP time adjust %s from %s (stratum %d)', offset, server, info.stratum))

        wifi.sta.disconnect()
        wifi.setmode(wifi.NULLMODE)

        reconnectAfter(3600000)
    end

    local function failure(err, s)
        status('SNTP failed')
        print(err, s)
        tmr.create():alarm(30000, tmr.ALARM_SINGLE, ntpSync)
    end

    sntp.sync({ '0.uk.pool.ntp.org', '1.uk.pool.ntp.org', '2.uk.pool.ntp.org', }, success, failure)
end

local function connectAccessPoint()
    status('Connecting...')
    wifi.setmode(wifi.STATION)
    wifi.sta.sethostname('lcd-clock')
    wifi.sta.connect()
end

local function scanForAccessPoint()
    local function ap_callback(ap)
        local ap_info = wifi.sta.getapinfo()

        if ap_info.qty == 0 then
            status('Set WiFi network')
            error('No WiFi access points configured')
        end

        for ap_index, config in ipairs(ap_info) do
            for ssid, _ in pairs(ap) do
                if ssid == config.ssid then
                    print(string.format('Found AP "%s"', ssid))
                    wifi.sta.changeap(ap_index)
                    connectAccessPoint()
                    return
                end
            end
        end

        status('No network found')
        tmr.create():alarm(60000, tmr.ALARM_SINGLE, scanForAccessPoint)
    end

    status('Scanning...')
    wifi.sta.getap(0, ap_callback)
end

local function initialise()
    local function staConnected()
        status('Wait for IP...')
    end

    local function staConnected(evt)
        if evt.reason then
            status('Connect failed')
            print('Disconnect reason ' .. evt.reason)
        end
    end

    local function staGotIp(evt)
        status(evt.IP)

        ntpSync()
    end

    local function staDhcpTimeout()
        status('DHCP timeout')

        wifi.setmode(wifi.NULLMODE)
        reconnectAfter(60000)
    end

    wifi.setmode(wifi.STATION)
    wifi.sta.sethostname('lcd-clock')

    wifi.eventmon.register(wifi.eventmon.STA_CONNECTED, staConnected)
    wifi.eventmon.register(wifi.eventmon.STA_DISCONNECTED, staDisconnected)
    wifi.eventmon.register(wifi.eventmon.STA_GOT_IP, staGotIp)
    wifi.eventmon.register(wifi.eventmon.STA_DHCP_TIMEOUT, staDhcpTimeout)

    reconnectTimer:register(1, tmr.ALARM_SEMI, connectAccessPoint)

    scanForAccessPoint()
end

function networkStart(cb)
    statusCallback = cb

    initialise()
end
