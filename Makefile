SERIAL = /dev/ttyUSB0

ESPTOOL = python3 tools/esptool/esptool.py --port $(SERIAL)
UPLOADER = python3 tools/nodemcu-uploader/nodemcu-uploader.py --port $(SERIAL)

FIRMWARE = nodemcu-master-11-modules-2020-05-17-12-56-23-integer.bin

error:
	@echo "Please choose one of the following target: id, flash, format, upload, terminal"
	@exit 1

id::
	$(ESPTOOL) flash_id
	$(ESPTOOL) read_mac

flash::
	$(ESPTOOL) write_flash -fm dio 0x00000 firmware/$(FIRMWARE)

format::
	$(UPLOADER) file format

upload::
	$(UPLOADER) upload *.lua

terminal::
	$(UPLOADER) terminal
