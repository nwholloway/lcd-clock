local function start()
    local function main()
        dofile('main.lua')
    end

    s, err = pcall(main)
    if not s then
        print(err)
    end
end

wifi.sta.autoconnect(0)

_, boot_reason = node.bootreason()
if boot_reason == 0 or boot_reason == 4 or boot_reason == 6 then
    node.setcpufreq(node.CPU80MHZ)
    print('LCD Clock')
    tmr.create():alarm(2000, tmr.ALARM_SINGLE, start)
else
    print('Not starting due to boot reason ' .. boot_reason);
end

