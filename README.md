LCD Clock
=========

This project uses NodeMCU firmware on an ESP8266 microcontroller to display a clock on an
1602 LCD panel connected via I2C board.

There are Arduino libraries (e.g. [LiquidCrystal](https://www.arduino.cc/en/Reference/LiquidCrystal) or
[LiquidCrystal_I2C](https://github.com/fdebrabander/Arduino-LiquidCrystal-I2C-library)) which
simplify driving the LCD, but I wanted to understand what the interface was like, and like using Lua.

Installation
------------

Clone this repository, along with the submodules used to tools used to flash the firmware
and upload code to microcontroller:
```shell script
git clone --recursive https://gitlab.com/nwholloway/lcd-clock
```

Install the Python libraries required by `esptool` and `nodemcu-uploader`:
```shell script
sudo apt install python3-serial python3-wrapt
```

Check USB port is correct:
```shell script
make id
```

Flash the firmware:
```shell script
make flash
```

Install the software:
```shell script
make upload
```

Configure the WiFi network:
```shell script
make terminal
> node.restore()
> wifi.sta.config({ ssid = 'WiFi', pwd = 'top secret' })
```
If you have multiple WiFi networks you want to be able to connect to, then this LUA
snippet can be used to configure them:
```lua
local ap_list = {
    { ssid = 'Primary AP', pwd = '********' },
    { ssid = 'Secondary AP', pwd = '******** },
}

wifi.sta.clearconfig()
wifi.sta.setaplimit(table.getn(ap_list))
for i, ap in ipairs(ap_list) do
    wifi.sta.changeap(i)
    wifi.sta.config(ap)
end
```
