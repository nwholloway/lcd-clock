dofile('lcd.lua')
dofile('time.lua')
dofile('network.lua')

local i2c_id = 0
local sda_pin = 2 -- D2/GPIO4
local scl_pin = 1 -- D1/GPIO5
local lcd_address = 63

local blanks = '               '
local months = 'JanFebMarAprMayJunJulAugSepOctNovDec'
local weekdays = 'SunMonTueWedThuFriSat'

local function clock()
    lcd:backlight(false)
    lcd:clear()

    local sec, day = -1, -1

    local function tick()
        local tm = localtime()

        if tm.sec == sec then
            return
        end
        sec = tm.sec

        lcd:moveTo(4, 0)
        lcd:print(string.format('%02d:%02d:%02d', tm.hour, tm.min, tm.sec))

        if tm.day == day then
            return
        end
        day = tm.day

        local weekday = weekdays:sub(tm.wday * 3 - 2, tm.wday * 3)
        local month = months:sub(tm.mon * 3 - 2, tm.mon * 3)

        lcd:moveTo(0, 1)
        lcd:print(string.format('%s %2d %s %4d', weekday, tm.day, month, tm.year))
    end

    tmr.create():alarm(100, tmr.ALARM_AUTO, tick)
end

local function isInitialised()
    epoch, _, _ = rtctime.get()

    return epoch > 0
end

local function showStatus(text)
    if not isInitialised() then
        lcd:moveTo(0, 1)
        lcd:print(text)
        lcd:print(blanks:sub(1, 16 - text:len()))
    end
end

if i2c.setup(i2c_id, sda_pin, scl_pin, i2c.SLOW) == 0 then
    error('Failed to initialise I2C')
end

lcd = LCD.create(i2c_id, lcd_address, true)
lcd:print('LCD Clock')

networkStart(showStatus)

-- Start clock when RTC has been set
tmr.create():alarm(1000, tmr.ALARM_AUTO, function(timer)
    if isInitialised() then
        timer:unregister()
        clock()
    end
end)
