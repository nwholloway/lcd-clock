-- In the UK the clocks go forward 1 hour at 1am on the last Sunday in March,
-- and back 1 hour at 2am on the last Sunday in October.
local function isDst(tm)
    -- Trivially false in Jan, Feb, Nov, Dev
    if tm.mon < 3 or tm.mon > 10 then
        return false
    end

    -- Trivially true in Apr, May, Jun, Jul, Aug, Sep
    if tm.mon > 3 and tm.mon < 10 then
        return true
    end

    -- Are we before or after 01:00 UTC on the last Sunday of the month
    local prev_sunday = tm.day - tm.wday + 1
    local after_switch = prev_sunday >= 25 and (tm.wday ~= 1 or tm.hour >= 1)

    return (tm.mon == 3 and after_switch) or (tm.mon == 10 and not after_switch)
end

function localtime()
    local epoch, _, _ = rtctime.get()
    local tm = rtctime.epoch2cal(epoch)
    if isDst(tm) then
        tm = rtctime.epoch2cal(epoch + 3600)
    end
    return tm
end
