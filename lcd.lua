-- LCD.create(i2c_id, address, two_line, large_chars)
--     Create LCD instance
--       i2c_id      - i2c bus number
--       address     - i2c device address
--       two_line    - is two line display
--       large_chars - use large chars (on one line display)
-- LCD:display(enable)
--     Display on/off
-- LCD:backlight(enable)
--     Backlight on/off
-- LCD:cursor(enable)
--     Cursor on/off
-- LCD:blink(enable)
--     Blinking mode on/off
-- LCD:clear()
--     Clear display
-- LCD:home()
--     Move to home position
-- LCD:moveTo(x, y)
--     Move to position
-- LCD:print(s)
--     Display string on display
-- LCD:cursorLeft()
--     Move cursor left
-- LCD:cursorRight()
--     Move cursor right
-- LCD:scrollLeft()
--     Scroll display left
-- LCD:scrollRight()
--     Scroll display right

-- Clear Display
local CMD_CLEAR_DISPLAY = 0x01
-- Return Home
local CMD_RETURN_HOME = 0x02
-- Entry Mode Set
local CMD_ENTRY_MODE_SET = 0x04
    local ENTRY_MODE_SCROLL = 0x01
    local ENTRY_MODE_LTR = 0x02
-- Display On/Off Control
local CMD_DISPLAY_CONTROL = 0x08
    local DISPLAY_BLINK = 0x01
    local DISPLAY_CURSOR = 0x02
    local DISPLAY_ON = 0x04
-- Cursor or Display Shift
local CMD_CURSOR_SHIFT = 0x10
    local CURSOR_SHIFT_RIGHT = 0x4
    local CURSOR_SHIFT_DISPLAY = 0x8
-- Function Set
local CMD_FUNCTION_SET = 0x20
    local FUNCTION_5x10DOTS = 0x04
    local FUNCTION_2LINE = 0x08
    local FUNCTION_8BITMODE = 0x10
-- Set CG RAM Address
local CMD_SET_CG_RAM_ADDR = 0x40
-- Set DD RAM Address
local CMD_SET_DD_RAM_ADDR = 0x80

-- Control Bits
local CTRL_RS = 0x01 -- Instruction/Data
local CTRL_RW = 0x02 -- Read/Write
local CTRL_EN = 0x04 -- Enable
local CTRL_BL = 0x08 -- Backlight

LCD = {}
LCD.__index = LCD

function LCD.create(i2c_id, address, two_line, large_chars)
    local self = {
        i2c_id = i2c_id,
        address = address,
        _backlight = CTRL_BL,
        _displayControl = bit.bor(DISPLAY_ON),
        _entryModeSet = bit.bor(ENTRY_MODE_LTR),
        _offset = 0
    }
    lcd = setmetatable(self, LCD)
    lcd:_init(two_line, large_chars)
    return lcd
end

local function setflag(value, flag, enable)
    return enable and bit.bor(value, flag) or bit.band(value, bit.bnot(flag))
end

function LCD:display(enable)
    self._displayControl = setflag(self._displayControl, DISPLAY_ON, enable)
    self:_setDisplayControl()
end

function LCD:backlight(enable)
    self._backlight = enable and CTRL_BL or 0
    self:_i2c_write(bit.bor(0, self._backlight))
end

function LCD:cursor(enable)
    self._displayControl = setflag(self._displayControl, DISPLAY_CURSOR, enable)
    self:_setDisplayControl()
end

function LCD:blink(enable)
    self._displayControl = setflag(self._displayControl, DISPLAY_BLINK, enable)
    self:_setDisplayControl()
end

function LCD:clear()
    self._offset = 0
    self:_instruction(CMD_CLEAR_DISPLAY)
end

function LCD:home()
    self._offset = 0
    self:_instruction(CMD_RETURN_HOME)
end

function LCD:moveTo(x, y)
    ram_addr = 0x40 * y + (x + self._offset) % 40
    self:_instruction(bit.bor(CMD_SET_DD_RAM_ADDR, ram_addr))
end

function LCD:print(str)
    for i = 1, string.len(str) do
        self:_data(string.byte(str, i))
    end
end

function LCD:cursorLeft()
    self:_instruction(bit.bor(CMD_CURSOR_SHIFT))
end

function LCD:cursorRight()
    self:_instruction(bit.bor(CMD_CURSOR_SHIFT, CURSOR_SHIFT_RIGHT))
end

function LCD:scrollLeft()
    self._offset = self._offset + 1
    self:_instruction(bit.bor(CMD_CURSOR_SHIFT, CURSOR_SHIFT_DISPLAY))
end

function LCD:scrollRight()
    self._offset = self._offset - 1
    self:_instruction(bit.bor(CMD_CURSOR_SHIFT, CURSOR_SHIFT_DISPLAY, CURSOR_SHIFT_RIGHT))
end

-- Implementation

function LCD:_init(two_line, large_chars)
    -- Set 4-bit mode
    self:_send_nibble(bit.bor(0x30, self._backlight))
    tmr.delay(4500)
    self:_send_nibble(bit.bor(0x30, self._backlight))
    tmr.delay(150)
    self:_send_nibble(bit.bor(0x20, self._backlight))

    -- Set display function
    displayFunction = two_line and FUNCTION_2LINE or large_chars and FUNCTION_5x10DOTS or 0
    self:_instruction(bit.bor(CMD_FUNCTION_SET, displayFunction))

    -- Initialise display
    self:_setDisplayControl()
    self:_setEntryModeSet()

    self:clear()
    self:home()
end

function LCD:_setDisplayControl()
    self:_instruction(bit.bor(CMD_DISPLAY_CONTROL, self._displayControl))
end

function LCD:_setEntryModeSet()
    self:_instruction(bit.bor(CMD_ENTRY_MODE_SET, self._entryModeSet))
end

function LCD:_setDisplayMode()
    self:_instruction(bit.bor(CMD_DISPLAY_CONTROL, self._entryModeSet))
end

function LCD:_instruction(data)
    self:_send_nibbles(data, 0)
end

function LCD:_data(data)
    self:_send_nibbles(data, CTRL_RS)
end

function LCD:_send_nibbles(data, mode)
    local hi = bit.band(data, 0xF0)
    local lo = bit.lshift(bit.band(data, 0x0F), 4)
    self:_send_nibble(bit.bor(hi, self._backlight, mode))
    self:_send_nibble(bit.bor(lo, self._backlight, mode))
end

function LCD:_send_nibble(data)
    self:_i2c_write(data)
    self:_i2c_write(bit.bor(data, CTRL_EN))
    tmr.delay(1)
    self:_i2c_write(data)
    tmr.delay(50)
end

function LCD:_i2c_write(data)
    i2c.start(0)
    i2c.address(self.i2c_id, self.address, i2c.TRANSMITTER)
    i2c.write(self.i2c_id, data)
    i2c.stop(0)
end
